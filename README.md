Hello there, this is my assignment From The Odin Project's [curriculum] .
You can find more information about it on...
(http://www.theodinproject.com/courses/web-development-101/lessons/html-css)

This is a remake of the google homepage, using only HTML&CSS.
The project may be enhanced in the future, because I am still
a beginner web-developer, and as new things come to my mind I will 
be developing this project.

I know this project isn't really much for some developers, but it's
a gold mine for beginners and I will try and do my best to make it as 
cool as possible. And bug free of course.

If you would like to share this project with me, feel free to join.


